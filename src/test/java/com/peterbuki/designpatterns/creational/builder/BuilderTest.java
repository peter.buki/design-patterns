package com.peterbuki.designpatterns.creational.builder;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class BuilderTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private Builder builder;

    @Before
    public void setup() {
        builder = new Builder();
    }

    @Test
    public void buildAgricultural_throwsException() throws Exception {
        expectedException.expect(UnsupportedOperationException.class);
        expectedException.expectMessage("Unsupported Building type Optional[Agricultural], stories 1, hasGarden false");
        Building building = builder.withNumberOfStories(1).withBuildingType(Builder.BuildingType.Agricultural).build();
    }

    @Test
    public void buildCottage() throws Exception {
        Building building = builder
                .withBuildingType(Builder.BuildingType.Residential)
                .withNumberOfStories(2)
                .withGarden()
                .build();
        assertTrue(building instanceof Cottage);
        Cottage cottage = (Cottage) building;
        assertTrue(cottage.getHasGarden());
        assertEquals(2, cottage.getNumberOfStories());
    }

    @Test
    public void buildWarehouse() throws Exception {
        Building building = builder
                .withBuildingType(Builder.BuildingType.Industrial)
                .withTruckTerminals()
                .withNumberOfStories(1)
                .build();
        assertTrue(building instanceof Warehouse);
        Warehouse warehouse = (Warehouse) building;
        assertTrue(warehouse.getHasTruckTerminals());
        assertEquals(1, warehouse.getNumberOfStories());
    }

}