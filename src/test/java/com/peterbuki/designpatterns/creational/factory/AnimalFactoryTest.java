package com.peterbuki.designpatterns.creational.factory;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AnimalFactoryTest {

    private AnimalFactory factory = new AnimalFactory();

    @Test
    public void createCatAndDog_success() {
        Animal cat = factory.create();
        Animal dog = factory.create();
        assertEquals("Bark!", dog.yell());
        assertEquals("Meow!", cat.yell());
    }
}