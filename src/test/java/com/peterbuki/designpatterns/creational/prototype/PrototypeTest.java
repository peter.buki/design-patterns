package com.peterbuki.designpatterns.creational.prototype;

import org.junit.Test;

import static org.junit.Assert.*;

public class PrototypeTest {

    @Test
    public void cloneTest() {
        Prototype prototype1 = new Prototype();
        Prototype prototype2 = new Prototype();
        assertNotEquals(prototype1.getRandomPrivateField(), prototype2.getRandomPrivateField());
        prototype2 = prototype1.clone();
        assertEquals(prototype1.getRandomPrivateField(), prototype2.getRandomPrivateField(), 0);
    }

}