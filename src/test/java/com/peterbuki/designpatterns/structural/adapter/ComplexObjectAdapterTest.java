package com.peterbuki.designpatterns.structural.adapter;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ComplexObjectAdapterTest {

    @Test
    public void doSomethingSimple() {
        SimpleAction simpleObject = new SimpleObject();
        String actionResult = simpleObject.doSomething();
        assertEquals("Did something.", actionResult);
    }

    @Test
    public void doSomethingComplex() {
        SimpleAction complexObjectAdapter = new ComplexObjectAdapter();
        String actionResult = complexObjectAdapter.doSomething();
        assertEquals("Did something complex very quickly.", actionResult);
    }
}