package com.peterbuki.designpatterns.structural.composite;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class HolderTest {

    @Test
    public void doSomeWork() {
        Holder holder = new Holder();
        holder.add(new ConcreteType("first"));
        holder.add(new ConcreteType("second"));
        Holder innerHolder = new Holder();
        innerHolder.add(new ConcreteType("third"));
        holder.add(innerHolder);

        String resultOfWork = holder.doSomeWork();
        assertEquals("Concrete first work. Concrete second work. Concrete third work.", resultOfWork);
    }

}