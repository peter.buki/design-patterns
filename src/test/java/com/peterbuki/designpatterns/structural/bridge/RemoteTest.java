package com.peterbuki.designpatterns.structural.bridge;

import org.junit.Test;

import static org.junit.Assert.*;

public class RemoteTest {

    @Test
    public void togglePower() {
        TV tv = new TV();
        Remote remote = new Remote(tv);

        assertFalse(tv.isEnabled());
        remote.togglePower();
        assertTrue(tv.isEnabled());
        remote.togglePower();
        assertFalse(tv.isEnabled());
    }
}