package com.peterbuki.designpatterns.structural.composite;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Holder implements Action {

    private List<Action> items = new ArrayList<>();

    public void add(Action action) {
        items.add(action);
    }

    @Override
    public String doSomeWork() {
        return items.stream().map(Action::doSomeWork).collect(Collectors.joining(" "));
    }
}
