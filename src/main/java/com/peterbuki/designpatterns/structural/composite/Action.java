package com.peterbuki.designpatterns.structural.composite;

public interface Action {
    String doSomeWork();
}
