package com.peterbuki.designpatterns.structural.composite;

public class ConcreteType implements Action {
    private String name;

    public ConcreteType(String name) {
        this.name = name;
    }

    @Override
    public String doSomeWork() {
        return String.format("Concrete %s work.", name);
    }
}
