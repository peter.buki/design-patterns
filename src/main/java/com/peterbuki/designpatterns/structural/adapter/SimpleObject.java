package com.peterbuki.designpatterns.structural.adapter;

public class SimpleObject implements SimpleAction {
    @Override
    public String doSomething() {
        return "Did something.";
    }
}
