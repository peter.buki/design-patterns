package com.peterbuki.designpatterns.structural.adapter;

public class ComplexObjectAdapter implements SimpleAction {
    private ComplexObject complexObject;

    public ComplexObjectAdapter() {
        complexObject = new ComplexObject();
    }

    @Override
    public String doSomething() {
        return complexObject.doSomething("quickly");
    }
}
