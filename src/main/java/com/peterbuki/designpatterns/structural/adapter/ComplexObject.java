package com.peterbuki.designpatterns.structural.adapter;

public class ComplexObject {
    public String doSomething(String how) {
        return String.format("Did something complex very %s.", how);
    }
}
