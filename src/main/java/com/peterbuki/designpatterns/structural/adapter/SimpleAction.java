package com.peterbuki.designpatterns.structural.adapter;

public interface SimpleAction {
    String doSomething();
}
