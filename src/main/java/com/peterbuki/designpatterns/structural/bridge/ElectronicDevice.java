package com.peterbuki.designpatterns.structural.bridge;

/**
 * Only one aspect of the implementation is done here.
 */
public abstract class ElectronicDevice implements Device {
    private boolean isEnabled = false;

    public boolean isEnabled() {
        return isEnabled;
    }

    public void powerOn() {
        isEnabled = true;
    }

    public void powerOff() {
        isEnabled = false;
    }
}
