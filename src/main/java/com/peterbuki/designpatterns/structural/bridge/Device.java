package com.peterbuki.designpatterns.structural.bridge;

public interface Device {
    boolean isEnabled();
    void powerOn();
    void powerOff();
}
