package com.peterbuki.designpatterns.structural.bridge;

/**
 * This is the actual bridge. One aspect of the implementation is done here.
 */
public class Remote {
    private Device device;

    public Remote(Device device) {
        this.device = device;
    }

    public void togglePower() {
        if (device.isEnabled()) {
            device.powerOff();
        }
        else
        {
            device.powerOn();
        }
    }
}
