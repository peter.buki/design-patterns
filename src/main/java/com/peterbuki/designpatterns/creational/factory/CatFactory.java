package com.peterbuki.designpatterns.creational.factory;

public class CatFactory implements Factory {
    @Override
    public Animal create() {
        return new Cat();
    }
}
