package com.peterbuki.designpatterns.creational.factory;

public class Cat implements Animal {

    @Override
    public String walk() {
        return "Walking like a cat.";
    }

    @Override
    public String yell() {
        return "Meow!";
    }
}
