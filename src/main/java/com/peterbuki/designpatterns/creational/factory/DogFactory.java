package com.peterbuki.designpatterns.creational.factory;

public class DogFactory implements Factory {
    @Override
    public Animal create() {
        return new Dog();
    }
}
