package com.peterbuki.designpatterns.creational.factory;

public class Dog implements Animal {
    @Override
    public String walk() {
        return "Walking like a dog.";
    }

    @Override
    public String yell() {
        return "Bark!";
    }
}
