package com.peterbuki.designpatterns.creational.factory;

public interface Animal {
    String walk();

    String yell();
}
