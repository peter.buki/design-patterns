package com.peterbuki.designpatterns.creational.factory;

public interface Factory {
    Animal create();
}
