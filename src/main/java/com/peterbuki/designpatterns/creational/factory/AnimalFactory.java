package com.peterbuki.designpatterns.creational.factory;

/**
 * Yes, sounds weird, but we will produce animals.
 */
public class AnimalFactory implements Factory {
    private static boolean nextAnimalIsCat = true;
    Factory catFactory = new CatFactory();
    Factory dogFactory = new DogFactory();

    @Override
    public Animal create() {
        Animal createdAnimal;
        if (nextAnimalIsCat) {
            createdAnimal = catFactory.create();
        } else {
            createdAnimal = dogFactory.create();
        }
        nextAnimalIsCat = !nextAnimalIsCat;
        return createdAnimal;
    }
}
