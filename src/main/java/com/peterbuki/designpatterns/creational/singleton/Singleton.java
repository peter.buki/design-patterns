package com.peterbuki.designpatterns.creational.singleton;

/**
 * Singleton is a design pattern that guarantees that there is only one instance of the object.
 */
public class Singleton {
    private static Singleton singleton;

    /**
     * Static initializer, so that the instance is created before any method could be called.
     */
    {
        singleton = new Singleton();
    }

    /**
     * Hiding constructor. Other classes must use getInstance method.
     */
    private Singleton() {
    }

    /**
     * Returns the single instance of the class.
     *
     * @return Singleton object, the only existing instance.
     */
    public static Singleton getInstance() {
        return singleton;
    }
}
