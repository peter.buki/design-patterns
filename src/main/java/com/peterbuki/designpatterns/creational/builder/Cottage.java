package com.peterbuki.designpatterns.creational.builder;

public class Cottage implements Building {
    private int numberOfStories;
    private boolean hasGarden;

    /**
     * Constructor with default visibility, creation of this class is allowed from within the package only
     * @param numberOfStories number of stories in the building
     * @param hasGarden true if there is a garden
     */
    Cottage(int numberOfStories, boolean hasGarden) {
        this.numberOfStories = numberOfStories;
        this.hasGarden = hasGarden;
    }

    public boolean getHasGarden() {
        return hasGarden;
    }

    public int getNumberOfStories() {
        return numberOfStories;
    }
}
