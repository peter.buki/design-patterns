package com.peterbuki.designpatterns.creational.builder;

import com.sun.javaws.exceptions.InvalidArgumentException;

import java.util.Optional;

/**
 * Using the design pattern name for the class names.
 * Usually it is called something like HouseBuilder.
 */
public class Builder {
    private int numberOfStories;
    private Optional<BuildingType> buildingType;
    private boolean hasGarden;
    private boolean hasTruckTerminals;

    /**
     * Enum for the different types of the buildings
     */
    public enum BuildingType {
        Residential, Commercial, Industrial, Infrastructure, Agricultural, Specialty;
    }

    /**
     * BuildingType must be specified.
     * @param buildingType type of the building to be built
     * @return the Builder object
     */
    public Builder withBuildingType(BuildingType buildingType) {
        this.buildingType = Optional.of(buildingType);
        return this;
    }

    /**
     * Number of stories of the building must be specified.
     * @param numberOfStories
     * @return the Builder object
     */
    public Builder withNumberOfStories(int numberOfStories) {
        this.numberOfStories = numberOfStories;
        return this;
    }

    /**
     * Call if the requested building has a garden.
     * An industrial building must not have a garden!
     * @return the Builder object
     */
    public Builder withGarden() {
        this.hasGarden = true;
        return this;
    }

    /**
     * Call if the requested building has truck terminals.
     * Only Industrial buildings can have such a feature.
     * @return the Builder object
     */
    public Builder withTruckTerminals() {
        this.hasTruckTerminals = true;
        return this;
    }

    /**
     * Verifies the parameters set before and builds the object.
     * @return the Building built with the feature set specified beforehand
     * @throws InvalidArgumentException If numberOfStories or BuildingType not specified or invalid combination.
     * @throws UnsupportedOperationException If unsupported object is requested.
     */
    public Building build() throws InvalidArgumentException, UnsupportedOperationException {
        if (numberOfStories == 0 || !buildingType.isPresent()) {
            throw new InvalidArgumentException(new String[]{"Must specify number of stories and type."});
        }
        else if (buildingType.get().equals(BuildingType.Residential)) {
            return buildCottage();
        }

        else if (buildingType.get().equals(BuildingType.Industrial)) {
            return buildWarehouse();
        }
        else {
            throw new UnsupportedOperationException(String.format("Unsupported Building type %s, stories %d, hasGarden %b",
                    buildingType, numberOfStories, hasGarden));
        }
    }

    /**
     * Verifies parameters for Residential buildings and builds a nice cottage.
     * @return the Cottage built
     * @throws InvalidArgumentException if invalid option was specified
     */
    private Building buildCottage() throws InvalidArgumentException {
        if (hasTruckTerminals) {
            throw new InvalidArgumentException(new String[]{"A residential building must have a truck terminal."});
        }
        Cottage cottage = new Cottage(numberOfStories, hasGarden);
        // do some additional checks and initialization
        return cottage;
    }

    /**
     * Verifies parameters and builds a nice Warehouse.
     * @return the new industrial Building
     * @throws InvalidArgumentException if invalid option or combination of options presented
     */
    private Building buildWarehouse() throws InvalidArgumentException {
        if (hasGarden) {
            throw new InvalidArgumentException(new String[]{"An industrial building must not have a garden."});
        }
        Warehouse warehouse = new Warehouse(numberOfStories, hasTruckTerminals);
        // do some additional checks and initialization
        return warehouse;
    }

}
