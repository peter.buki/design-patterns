package com.peterbuki.designpatterns.creational.builder;

public class Warehouse implements Building {
    private int numberOfStories;
    private boolean hasTruckTerminals;

    /**
     * Constructor with default visibility, creation of this class is allowed from within the package only
     * @param numberOfStories number of stories in the building
     */
    Warehouse(int numberOfStories, boolean hasTruckTerminals) {
        this.numberOfStories = numberOfStories;
        this.hasTruckTerminals = hasTruckTerminals;
    }

    /**
     * getter for the number of stories - it should be implemented in an abstract class.
     * @return the number of stories
     */
    public int getNumberOfStories() {
        return numberOfStories;
    }

    public boolean getHasTruckTerminals() {
        return hasTruckTerminals;
    }
}
