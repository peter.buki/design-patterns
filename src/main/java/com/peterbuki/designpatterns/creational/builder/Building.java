package com.peterbuki.designpatterns.creational.builder;

public interface Building {
    int getNumberOfStories();
}
