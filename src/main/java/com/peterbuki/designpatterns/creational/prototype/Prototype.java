package com.peterbuki.designpatterns.creational.prototype;

public class Prototype implements Cloneable {
    private double randomPrivateField;

    public Prototype() {
        randomPrivateField = Math.random();
    }

    public double getRandomPrivateField() {
        return randomPrivateField;
    }

    @Override
    public Prototype clone() {
        Prototype clonedPrototype = new Prototype();
        clonedPrototype.randomPrivateField = randomPrivateField;
        return clonedPrototype;
    }


}
